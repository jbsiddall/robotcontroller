
import uk.ac.warwick.dcs.maze.logic.IRobot;
import java.util.*;
import java.awt.Point;


public class Enterprise
{
    private List<Node> nodes; //list of all encountered nodes for this maze
    private List<Edge> edges; //list of all relationships between known nodes for this maze
    private Stack<Integer> stack; //list of headings to take at each consecutive junction/crossroads to reach destination
    private Edge currentEdge; //the ongoing edge between the last node visited and the next junction/node to be visited
    private boolean backtracking; //wheather the robot is exploring still or is using the stack to get to a destination
    private boolean targetFound; //true = the target has been reached
    private boolean reachTarget; //false = no path will exist between robot and maze
    public IRobot robot; //the main robot in the maze
    public final int[] allDirections = {IRobot.AHEAD, IRobot.BEHIND, IRobot.RIGHT, IRobot.LEFT}; //array of all directions
    public final int[] allHeadings = {IRobot.NORTH, IRobot.SOUTH, IRobot.EAST, IRobot.WEST}; //array of all headings

    public void controlRobot(IRobot robot)
    {
        System.out.println("ControlRobot called");
        this.robot = robot; //assigns the public robot to the robot passed 
        if(adjacent(allDirections, IRobot.BEENBEFORE, false,false).length == 0) //beenbefore Count = 0?
        {
            if(robot.getRuns() == 0) //first run of a new maze
            {

                newMaze(); // resets all global variables and maze data
                addNode(robot.getLocation(),adjacent(allHeadings,IRobot.PASSAGE, false,true)); // adds start position as a special node
                currentEdge = new Edge(searchNode(robot.getLocation()), true); //assigns currentEdge
                if(adjacent(allDirections, IRobot.PASSAGE, false, false).length == 0) //passage count == 0?
                {
                    reachTarget = false; //then target cannot move, and cannot reach target
                }
                else
                {

                    robot.setHeading(currentEdge.getA().getHeadings().lastElement().intValue()); //Random Direction - that isn't walled
                    updateCurrentEdge(null,robot.getHeading(),IRobot.CENTRE); //updates current edge's heading from A to B
                    robot.advance();
                }
            }
            else //new run, pathfind to nearest unexplored path adjacent to a node
            {
                newRun();
                backtracking = true;
                Node nearest = nearestNode();
                if(nearest != null) //unexplored node exists?
                {
                    pathFinder(searchNode(robot.getLocation()), searchNode(nearest.getLocation())); //backtracks to nearest unexplored junction
                }
                else
                {
                    if(targetFound)
                    {
                        pathFinder(searchNode(robot.getLocation()), searchNode(robot.getTargetLocation())); //backtracks to target
                    }
                    else
                    {
                        reachTarget = false; //then target cannot be reached, and therefore should spin on spot to indicate this
                    }
                }
            }
        }


        if(!reachTarget) // can robot reach Target?
        {
            spin(); // if not then spins the robot on its location
        }
        else // robot can possibly reach target
        {
            if(currentEdge != null) // CurrentEdge null?
            {
                currentEdge.incrementLength(); //increments the length of the edge between active nodes
            }

            if(targetAdjacent() && (adjacent(allDirections, IRobot.WALL, true, false).length <= 2)) //target is adjacent to robot and robot is not at junction
            {

                if(!targetFound) //target not in nodes?
                {
                    int[] noHeadings = {};
                    addNode(robot.getTargetLocation(), noHeadings); //adds target as a new node
                }
                targetFound = true; //target has been found
                updateCurrentEdge(searchNode(robot.getTargetLocation()),IRobot.CENTRE, opositeHeading(robot.getHeading())); //sets the currrent edge's final node as target node
                currentEdge.setRoutable(false);
                addEdge(currentEdge);

            }

            controllers(); //calls a controller based on exit count
                    System.out.println(" " + nodes.size());
        }
    }


    private void newMaze() //resets data when a new maze has started
    {
                System.out.println("newMaze called");
        nodes = new LinkedList<Node>(); //resets all the stored nodes
        edges = new LinkedList<Edge>(); //resets all stored edges
        targetFound = false;
        reachTarget = true;
        newRun();
    }

    private void newRun() //resets data when a new run has started, but still the same maze
    {
                System.out.println("newRun called");
        stack = new Stack<Integer>(); //resets the stack
        currentEdge = null; //resets the currentEdge
        backtracking = false; //resets backtracking to "explore mode"
    }

    private void addNode(Point location, int[] unexploredHeadings) // adds a new node to list
    {
                System.out.println("addNode called");
        nodes.add(new Node(location, unexploredHeadings));
    }

    private void addEdge(Edge newEdge) // adds a new edge to the list
    {
                System.out.println("addEdge called");
        edges.add(newEdge);
    }

    private void updateCurrentEdge(Node nodeB, int headingAB, int headingBA) // updates currentEdge with new parameters
    {
                System.out.println("updateCurrentEdge called");
        if(nodeB != null){this.currentEdge.setB(nodeB);} //updates node B
        if(headingAB != IRobot.CENTRE){this.currentEdge.setHeadingToB(headingAB);} //updates heading to get to B
        if(headingBA != IRobot.CENTRE){this.currentEdge.setHeadingToA(headingBA);} //updates heading to get to A

    }

    private void spin() // spins robot on spot if cannot reach target
    {
                System.out.println("spin called");
        move(IRobot.LEFT);
    }

    private void controllers() //calls a controller based on exit count
    {
                System.out.println("controllers called");
        int exits = adjacent(allDirections, IRobot.WALL, true, false).length; //number of exits based on current position
        int beenBefore = adjacent(allDirections, IRobot.BEENBEFORE, false, false).length; // number of beenbefore squares based on current position

        if((exits == 1) && (beenBefore > 0)) // is robot in deadend? true = yes
        {
                deadEndController(); //robot is at deadend
        }

        if((exits == 2) && (beenBefore > 0)) //is robot in corridor? true = yes
        {
            corridorController(); // robot is in corridor
        }

        if(exits >= 3) //is robot at a junction/crossroads? true = yes
        {
            junctionController();
        }
        
    }

    private void deadEndController() //called when at a deadend, reverse robot (exception at first move)
    {
                System.out.println("deadEndController called");
        move(IRobot.BEHIND);
    }

    private void corridorController() //called when in a corridor, will choose between ahead, left or right (exception at first move)
    {
                System.out.println("corridorController called");
        int[] possibleDirections = {IRobot.AHEAD, IRobot.LEFT, IRobot.RIGHT};
        int direction = randomDirection(adjacent(possibleDirections, IRobot.WALL, true, false)); //chooses a direction out of ahead, left or right, which isn't walled, randomly
        move(direction); //robot faces direction and advances.
    }

    private void junctionController() //called when in a junction/crossroads, will make choose heading based on multiple factors
    {
                System.out.println("junctionController called");
        if(backtracking) //Backtracking?
        {
            if(stack.size() == 0) //at target?
            {
                backtracking = false;
                junctionController();
            }
            else //not at target
            {
                robot.setHeading(stack.pop()); //pops next heading from stack
                robot.advance();
            }
        }
        else //not backtracking
        {
             Integer leaveHeading = new Integer(currentEdge.getHeadingToB()); //the heading when the robot left the node
             Integer enterHeading = new Integer(opositeHeading(robot.getHeading())); //the oposite heading to when the robot entered this node
             Node A = currentEdge.getA();
             Node B = searchNode(robot.getLocation());
            if(B == null) //New Junction?
            {
                int[] possibleUnexplored = adjacent(allHeadings,IRobot.PASSAGE,false,true); //list of all surrounding headings which head to unexplored passages
                List<Integer> unexplored = new LinkedList<Integer>(); //list of unexplored passages exculding the entry heading
                for(int i : possibleUnexplored)
                {
                    unexplored.add(new Integer(i));
                }
                unexplored.remove(new Integer(enterHeading)); //removes entry heading
                possibleUnexplored = new int[unexplored.size()];
                for(int i = 0; i < unexplored.size(); i++) //converts list back to array 
                {
                    possibleUnexplored[i] = unexplored.get(i).intValue();
                }

                addNode(robot.getLocation(), possibleUnexplored); //adds a new node
                B = searchNode(robot.getLocation()); //finds node B and assigns it to B

            }
            else //old junction
            {
                 B.getHeadings().remove(new Integer(enterHeading));
            }

            if(currentEdge != null) //Current Edge null?
            {
                if(A == B) //same node the edge started at?
                {
                    if(A.getHeadings().contains(leaveHeading)){A.getHeadings().remove(leaveHeading);} //removes the unexplored headings which lead strait back to it
                    if(A.getHeadings().contains(enterHeading)){A.getHeadings().remove(enterHeading);} //if the 2 exit and enter headings arnt the same then remove both of them
                    currentEdge = null;
                }
                else //at a different node to the start node in the edge
                {
                    Edge same = searchEdge(currentEdge.getA().getLocation(), robot.getLocation());
                    if(same == null) {same = searchEdge(robot.getLocation(), currentEdge.getA());}
                    if(same != null) //edge already exist?
                    {
                        if(currentEdge.getLength() < same.getLength()) //is this length smaller than the edge that already exists?
                        {
                            updateCurrentEdge(B, leaveHeading, enterHeading); //updates currentEdge with final node and headings
                            edges.set(edges.indexOf(same), currentEdge); //replaces existing edge with current Edge
                        }
                        else //same edge already exists which is shorter or the same
                        {
                            currentEdge = null;
                        }
                    }
                    else //edge doesnt already exist
                    {
                        updateCurrentEdge(B, leaveHeading, enterHeading); //updates currentEdge with final node and headings
                        addEdge(currentEdge); //adds current edge to edges
                    }
                }
            }
             
            if(adjacent(allDirections, IRobot.PASSAGE, false, false).length == 0) //Passage Count?
            {

                //passage Count is 0
                Node destination = nearestNode();
                if(destination != null) // unexplored node?
                {
                    backtracking = true;
                    pathFinder(searchNode(robot.getLocation()), destination);
                    junctionController();
                }
                else // no unexplored node
                {
                    if(targetFound) // target found?
                    {
                        backtracking = true;
                        pathFinder(searchNode(robot.getLocation()), searchNode(robot.getTargetLocation()));
                        junctionController();
                    }
                    else //target not found
                    {
                        reachTarget = false;
                    }
                }
                
            }
            else //passage count is > 0
            {
                 Node currentNode =searchNode(robot.getLocation());
                int heading = currentNode.getHeadings().lastElement();
                currentEdge = new Edge(currentNode, true);

                if(targetAdjacent()) //target is adjacent to robot and robot is at junction
                {
                    if(!targetFound) //target not in nodes?
                    {
                        int[] noHeadings = {};
                        addNode(robot.getTargetLocation(), noHeadings); //adds target as a new node
                    }
                    targetFound = true; //target has been found
                    A = currentEdge.getA(); //the new currentEdge will have the same node A
                    int ALength = currentEdge.getLength(); // the new currentEdge will have same length
                    currentEdge.incrementLength(); //increments lenght for target edge because target edge is 1 step away
                    updateCurrentEdge(searchNode(robot.getTargetLocation()),targetHeading(), opositeHeading(targetHeading()) ); //sets the currrent edge's final node as target node
                    currentEdge.setRoutable(false);
                    addEdge(currentEdge);
                    currentEdge = new Edge(A, true);
                    updateCurrentEdge(null, heading, IRobot.CENTRE);
                    currentEdge.setLength(ALength);
                }

                
                    robot.setHeading(heading);
                    robot.advance();
                
            }
             
        }
        
    }

    private void move(int direction) //faces the robot in the direction and advances it
    {
                                System.out.println("move called");
        robot.face(direction);
        robot.advance();
    }

    private boolean targetAdjacent() // returns true if the target is adjacent to robot
    {
                                System.out.println("targetAdjacent called");
        return (((int)distance(robot.getLocation(), robot.getTargetLocation())) == 1);
    }

    private int targetHeading() //returns the heading of the target relative to the robot
    {
                                System.out.println("targetHeading called");
        Point relative = new Point(robot.getTargetLocation().x - robot.getLocation().x, robot.getTargetLocation().y - robot.getLocation().y); //the coordinates of the target relative to robot
        int heading = IRobot.CENTRE; // if target is not adjacent to robot then return IRobot.CENTRE
        if(relative.x == 0)
        {
            if(relative.y > 0){heading = IRobot.NORTH;} //target is north of robot
            if(relative.y < 0){heading = IRobot.SOUTH;} //target is south of robot
        }

        if(relative.y == 0)
        {
            if(relative.x > 0){heading = IRobot.EAST;} //target is east of robot
            if(relative.x < 0){heading = IRobot.WEST;} //target is west of robot
        }

        return heading;
    }

    private Node searchNode(Point location) //returns a node with the coordinates passed
    {
                                System.out.println("searchNode called");
        Node result = null;
        for(int i = 0; i < nodes.size(); i++)
        {
            if((nodes.get(i).getLocation().x == location.x) && (nodes.get(i).getLocation().y == location.y))
            {
            
                result = nodes.get(i);
                break;
            }
        }
        return result;
    }

    private Edge searchEdge(Point A, Point B) //searches for an edge which matches both nodes
    {
                                System.out.println("searchEdge called");
        Edge result = null;
        for(int i = 0; i < edges.size(); i++)
        {
            if((edges.get(i).getA().getLocation().x == A.x) && (edges.get(i).getA().getLocation().y == A.y) && (edges.get(i).getB().getLocation().x == B.x) && (edges.get(i).getB().getLocation().y == B.y))
            {
                result = edges.get(i);
                break;
            }
        }
        return result;
    }

    private Node nearestNode() //returns the node which has an unexplored heading and is closest to robot
    {
                                System.out.println("nearestNode called");
        Node nearest = null;
        int nearestDistance = 0;
        for(int i = 0; i < nodes.size(); i++)
        {
            if(nodes.get(i).unexplored()) //only looks at nodes that have unexplored passages
            {
                int nodesDistance = (int)distance(robot.getLocation(), nodes.get(i).getLocation()); //gets the distance between robot and node
                if(nearest == null) //when nearest is null then assign it to the first node in the list
                {
                    nearest = nodes.get(i);
                    nearestDistance = nodesDistance;
                }
                else //compares currentNearest node with each node in list
                {
                    if(nearestDistance > nodesDistance) //this node is closer than current nearest, therefore update current nearest
                    {
                        nearest = nodes.get(i);
                        nearestDistance = nodesDistance;
                    }
                }
            }
        }
        return nearest;
    }

    private double distance(Point A, Point B) //returns the difference between the coordinates
    {
                                System.out.println("distance called");
        double diffX = Math.pow((double)(A.x - B.x), 2d);
        double diffY = Math.pow((double)(A.y - B.y), 2d);
        double difference = Math.sqrt(diffX + diffY);
        return difference;
    }


    private Edge getEdge(Node A, Node B) //searches for an edge which matches both nodes in either direction
    {
                                System.out.println("getEdge called");
        Edge result = null;
        for(int i = 0; i < edges.size(); i++)
        {
            if(((edges.get(i).getA() == A) && (edges.get(i).getB() == B)) || ((edges.get(i).getA() == B) && (edges.get(i).getB() == A)))
            {
                result = edges.get(i);
                break;
            }
        }
        return result;
    }

    
    private void pathFinder(Node start, Node finish) //updates the stack with a path between start and finish nodes
    {
                                System.out.println("pathFinder called");


        boolean target = (finish.getLocation() == robot.getLocation());
         Stack<Node> history = new Stack<Node>(); //all the nodes already visited
        if(scan(history, start, finish, target))
        {
            for(int i = history.size() - 2; i >= 0; i--)
            {
                Edge search = searchEdge(history.get(i), history.get(i + 1));
                if(search == null)
                {
                    search = searchEdge(history.get(i + 1), history.get(i));
                    stack.push(new Integer(search.getHeadingToA()));

                }
                else
                {
                    stack.push(new Integer(search.getHeadingToB()));
                }
            }



        }
        else
        {
            reachTarget = false;
        }

    }



    private boolean scan(Stack<Node> history, Node point, Node finish, boolean target) //recursivly looks at each node attempting to get to finish
    {
        Stack<Node> list = relations(point, target);
        history.add(point);
        
        for(int i = 0; i < list.size(); i++)
        {
            if(history.contains(list.get(i)))
            {
                int sum = 0;
                int indexStart = history.indexOf(point);
                int indexFinish = history.indexOf(list.get(i));
                for(int z = indexStart; z < indexFinish; z++)
                {
                    sum+= getEdge(history.get(z), history.get(z+1)).getLength();
                }

                if(sum > getEdge(point, list.get(i)).getLength())
                {
                    while(indexFinish - 1> indexStart)
                    {
                        indexFinish--;
                        history.remove(indexStart + 1);
                    }
                }
                list.remove(i);
            }
        }

        boolean success = (point == finish);

        for(int i = 0; i < list.size(); i++)
        {
            if(scan(history, list.get(i), finish, target))
            {
                success = true;
            }
        }
        
        return success;
    }

    private Stack relations (Node point, boolean target) //returns all the edges where point it one of the nodes
    {
        Stack<Node> list = new Stack<Node>();
        for(Edge i : edges)
        {
            if(target || i.getRoutable()) //either all edges are to be incldued or only include edges which are routable
            {
               if(i.getA() == point) //if A is a match to point
               {
                    list.add(i.getB());
               }
               else if(i.getB() == point) //if B is a match to point then swap over A and B and there headings
               {
                 list.add(i.getA());
               }
               else{}
            }
        }
        return list;
    }

    //returns all the directions which are or not the square passed based on the variable attract.
	private int[] adjacent(int[] directions,int square, boolean avoid, boolean headings)
	{
                                    System.out.println("adjacent called");
                int previousHeading = robot.getHeading(); //captures robots current heading
                List<Integer> adjacentDirections = new LinkedList<Integer>();
		for(int i = 0; i < directions.length; i++)
                {
                    boolean result = false;
                    if(headings) //2 different methods are used based on wheather the array passed contains headings or directions
                    {
                        robot.setHeading(directions[i]);
                        result = ((avoid && (robot.look(IRobot.AHEAD) != square)) || (!avoid && (robot.look(IRobot.AHEAD) == square)));
                    }
                    else
                    {
                        result = ((avoid && (robot.look(directions[i]) != square)) || (!avoid && (robot.look(directions[i]) == square)));
                    }

                    if(result){adjacentDirections.add(directions[i]);}
                }
                if(headings){robot.setHeading(previousHeading);} //restores the previous heading, because the heading will have been changed temporarily within this method
                //copies across all values in the adjacentDirection to a new array to be returnd
                int[] list = new int[adjacentDirections.size()];
                for(int i = 0; i < list.length;i++)
                {
                    list[i] = adjacentDirections.get(i).intValue();
                }

                return list;
	}

    //returns the oposite heading to the heading passed, ie north, will return south
    private int opositeHeading(int heading)
    {
        int oposite = IRobot.CENTRE;
        switch(heading)
        {
                case IRobot.NORTH: oposite = IRobot.SOUTH;break;
                case IRobot.SOUTH: oposite = IRobot.NORTH;break;
                case IRobot.EAST: oposite = IRobot.WEST;break;
                case IRobot.WEST: oposite = IRobot.EAST;break;
        }
        return oposite;
    }



    //chooses a random direction from the possible directions passed to it
    private int randomDirection(int[] directions)
    {
    int randno = (int)(Math.random() * directions.length);
    return directions[randno];
    }


}

















class Node extends Point // a junction, crossroads, or the special start and end points
{
   private Stack<Integer> unexploredHeadings; //all headings from this that are passages

    Node(Point location, int[] headings) //initilises location and unexploredHeadings
    {
        super();
        super.setLocation(location);
        this.unexploredHeadings  = new Stack<Integer>();
        for(int a : headings)
        {
            this.unexploredHeadings.add(new Integer(a));
        }
    }

    public boolean unexplored() //true = still passages from this that are unexplored, ie passage squares
    {
        return (unexploredHeadings.size() != 0);
    }

    public Stack<Integer> getHeadings() // returns the unexploredHeadings
    {
        return this.unexploredHeadings;
    }

}








class Edge // a relationship between two nodes
{
    private Node A; //the first node in the relationship
    private Node B; // the second node in the relationship
    private int headingAToB; //the heading when at A to take to reach B
    private int headingBToA; // the heading when at B to take to reach A
    private int length; // the number of steps/squares between node A and node B
    private boolean routable; //wheather robot can use this edge when traversing the network, this cannot be done if one of the edges is target destination

    Edge(Node a, boolean routable) //assigns the first node in the relationship
    {
        this.A = a;
        this.B = null;
        this.headingAToB = IRobot.CENTRE;
        this.headingBToA = IRobot.CENTRE;
        this.length = 0;
        this.routable = routable;
    }
    

    
    public void setB(Node b) //assigns the second node in the relationship
    {
        this.B = b;
    }

    public void setHeadingToB(int heading) //assigns a heading from A to get to B
    {
        this.headingAToB = heading;
    }

    public void setHeadingToA(int heading) // assigns a heading from B to get to A
    {
        this.headingBToA = heading;
    }

    public void setRoutable(boolean routable) //assigns a value to routable
    {
        this.routable = routable;
    }

    public void setLength(int length) //assigns a length to this edge
    {
        this.length = length;
    }

    public void incrementLength() //increments the length between both node A and B
    {
        this.length ++;
    }

    public boolean getComplete() //true = both nodes are assigned an
    {
        return ((A != null) && (B != null));
    }

    public Node getA() //returns node A
    {
        return this.A;
    }

    public Node getB() // returns node B
    {
        return this.B;
    }

    public int getLength() // gets the length between node A and node B
    {
        return this.length;
    }

    public int getHeadingToA() //returns the heading when at B to take to get to A
    {
        return this.headingBToA;
    }

    public int getHeadingToB() // returns the heading when at A to take to get to B
    {
        return this.headingAToB;
    }

    public boolean getRoutable() //wheather this edge doesnt lead to target (or start with target), true = not target
    {
        return this.routable;
    }


}
