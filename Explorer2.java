
import java.util.Stack;
import java.util.LinkedList;
import java.util.List;
import uk.ac.warwick.dcs.maze.logic.IRobot;

public class Explorer2
{
        //the number of adjacent passage squares on the previous step
        private int previousPassageCount;

	//all the junctions encountered
	private Stack<Integer> junctions;

        private IRobot robot; //allows all methods in this class access the robot
        public final int[] allDirections = {IRobot.AHEAD, IRobot.BEHIND, IRobot.RIGHT, IRobot.LEFT}; //array of all directions

	public void controlRobot(IRobot robot)
	{
            this.robot = robot;
            int beenBefore = adjacent(allDirections, IRobot.BEENBEFORE, false).length; //number of beenbefore squares adjacent to the robot
            int passage = adjacent(allDirections, IRobot.PASSAGE, false).length; //number of passage squares adjacent to the robot
            if(beenBefore == 0) {previousPassageCount = passage;} //updates the previous passage count if this is the first step
            if (passage > 0) {explore();} else {backtrack();} //explores if there there are passage squares available otherwise backtracks to last known passage square
            previousPassageCount = passage; //updates the previous passage count for next step
          }

	private void explore()
	{
            int exits = adjacent(allDirections, IRobot.WALL, true).length; //the number of adjacent squares which are traversable
            switch(exits)
            {
                case 1: deadend(); break; //responds to being at a deadend
                case 2: corridor(); break;//responds to being in a corridor
                default: junction(); //responds to being in a junction/cross-roads
            }
	}

	private void backtrack()
	{
            int exits = adjacent(allDirections, IRobot.WALL, true).length; //the number of adjacent squares which are traversable
            
            if(previousPassageCount == 0) //last step also didnt have any passages available
            {
                switch(exits)
                {
                    case 1: deadend(); break;
                    case 2: corridor(); break;
                    default: lookup();
                }
            }
            else
            {
                deadend();
            }
	}

        //pops the previous junctions and takes the heading from which the robot entered that junction
        private void lookup()
        {
            int heading = opositeHeading(junctions.pop()); //gets the exit heading from the last junction, and removes the last junction from the stack
            robot.setHeading(heading);
            robot.advance();
        }

        //robot responds to being at a deadend
        private void deadend()
        {
             int beenBefore = adjacent(allDirections, IRobot.BEENBEFORE, false).length; //the number of adjacent squares which are traversable
             if(beenBefore == 0){robot.face(randomDirection(adjacent(allDirections, IRobot.PASSAGE, false)));}
             else {robot.face(IRobot.BEHIND);}
             robot.advance();
        }

        //robot responds to being in a corridor
        private void corridor()
        {
            int[] availDir = {IRobot.AHEAD, IRobot.LEFT, IRobot.RIGHT}; //all the directions that can be taken in a corridor situation
            int direction = randomDirection(adjacent(availDir, IRobot.WALL, true)); //randomly chooses a direction out of availDir which isn't a wall
            robot.face(direction);
            robot.advance();
        }

        //robot resonds to being at a NEW junction
        private void junction()
        {
            if(previousPassageCount > 0){addJunction(robot.getHeading());}
            int direction = randomDirection(adjacent(allDirections, IRobot.PASSAGE, false)); //chooses a random direction out of the available passage squares
            robot.face(direction);
            robot.advance();
        }


	//called by the maze-environment, at the start of a new run.
	public void reset()
	{
	    junctions = new Stack<Integer>();
            previousPassageCount = 0;
	}
	

	//returns all the headings which are or not the square passed based on the variable attract.
	private int[] adjacent(int[] directions,int square, boolean avoid)
	{
                List<Integer> adjacentDirections = new LinkedList<Integer>();

		for(int i = 0; i < directions.length; i++)
                {
                    if((avoid && (robot.look(directions[i]) != square)) || (!avoid && (robot.look(directions[i]) == square)))
                    {
                        adjacentDirections.add(directions[i]);
                    }
                }
                //copies across all values in the adjacentDirection to a new array to be returnd
                int[] list = new int[adjacentDirections.size()];
                for(int i = 0; i < list.length;i++)
                {
                    list[i] = adjacentDirections.get(i).intValue();
                }

                return list;
	}


    //chooses a random direction from the possible directions passed to it
    private int randomDirection(int[] directions)
    {
    int randno = (int)(Math.random() * directions.length);
    return directions[randno];
    }

    //returns the oposite heading to the heading passed, ie north, will return south
    public int opositeHeading(int heading)
    {
        int oposite = IRobot.CENTRE;
        switch(heading)
        {
                case IRobot.NORTH: oposite = IRobot.SOUTH;break;
                case IRobot.SOUTH: oposite = IRobot.NORTH;break;
                case IRobot.EAST: oposite = IRobot.WEST;break;
                case IRobot.WEST: oposite = IRobot.EAST;break;
        }
        return oposite;
    }

    //adds a new junction to the list
    public void addJunction(int in)
    {
	junctions.push(in);
    }
}

